---
title: Legal
date: Sunday, 4 July 2021
dateiso: 2021-07-04
...

All views or opinions expressed on this site are mine and do not represent
those of people, institutions or organisations that I may or may not be
associated with, professional or otherwise.

Unless explicitly specified, all original content or material, including code
snippets, created by me is licensed under a [Creative Commons License (CC BY NC
SA)](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode).

[![CC BY NC SA](/assets/by-nc-sa.svg){tabindex="0" .img-fixed}](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode){tabindex="-1"}
