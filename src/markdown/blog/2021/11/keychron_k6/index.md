---
title: Keychron K6
date: 2021-11-28
...

Most of my time is spent in front of a screen. At home, I use a gaming
mechanical keyboard that I bought just after building my first PC. Now that
I've started working, having a mechanical keyboard to bring to the office would
be a nice upgrade.

This keyboard would need to meet some requirements. It would:

1. Need to be portable.
2. Need to be wireless.

I did not spend a lot of time researching. I saw that the most recommended
keyboard are the Keychron K2 and the Keychron K6 The Keychron K2 in my
preferred switches are not in stock. So I settled for a Keychron K6 with
Gateron Browns switches, aluminium frame and hot-swappable PCB.


## Timeline

1. Logitech G710+ (2015-present, Cherry MX Browns)
2. Ducky One 2 (2018-2020, Cherry MX Browns)
3. Razer BlackWidow 2013 (2020-present, Cherry MX Blues)
4. Keychron K6 (2021-present, Gateron Browns)


## Prelude

I have used Cherry MX Browns since my first mechanical keyboard. I did try out
MX Blues for a few months and they're fine, but I seem to gravitate back to
tactile switches.

I got into mechanical keyboards back in 2015, and then back a bit again in
2018, but I didn't go deep into the rabbit hole. Back then, all the hobby meant
to me was just getting a board with a specific feel of switches that ranges
from Cherrys to Kailh and swapping out the key caps for a touch of personality.

Coming back into it again in 2021, I found out that the mechanical keyboard
rabbit hole goes a lot deeper than I thought. Many new different switches,
plate material, case foam, lubrication, custom stabilisers; this scene has
really changed a lot and is very overwhelming.


## Keychron K6

### Unboxing

The packaging is nice and neatly presented, with foam surrounding the keyboard
and segments the extras that comes with the keyboard.

Included with the keyboard were several alternate keys to swap with which is
nice to have. These alternate keys include Windows keys, control-alt swap and
non-accented escape and light keys. Along side the extra key caps are the key
cap puller and a key switch puller, as this is the hot-swappable version.

<figure>
:::{.content-media-container}
![](k6-unbox-1.png "Keychron K6 box")
:::

:::{.content-media-container}
![](k6-unbox-2.png "Keychron K6")
:::
</figure>


### Size and Feel

The smallest keyboard I've owned prior to the Keychron K6 is the Ducky One 2,
which was an 80% TKL layout. I originally bought for the purpose to bring to
university. However, I still found the form factor to be too large and not
portable to be carrying around on a daily basis. I had to bring it around using
the keyboard's box and did not see much use.

The Keychron K6 is a 65% layout. It omits the Function Row, parts of the
Navigation Cluster, merges Escape Key with Tilde and Backtick and compresses
the Arrow Cluster with the bottom right section of the keyboard. This results
in a very portable keyboard that I can comfortably bring around in my bag.

The board feels solid in the hands with not much flex. The aluminium frame
helps add the rigidity of the case and puts on a fair amount of weight. There
is a variant of the K6 without the aluminium frame. I would recommend getting
the aluminium frame as it improves the keyboard tremendously. Without the
aluminium frame, there would be some odd grooves along the top and bottom of
the keyboard where the frame would slide into.

The switches feels different to their Cherry counterparts, despite being based
off them. I can't say with 100% certainty if there is an actual difference
between the switches. The only tactile keyboard that I can compare against is
the Logitech G710+. They have different feel and sounds, of which I think are
caused by the case of keyboard. The Keychron feels slightly more tactile than
the Logitech however.


### Interfaces

There are 2 ways to connect the Keychron K6; wirelessly using Bluetooth and
wired using the included USB C cable.

The USB C port is located on the left side of the keyboard. I am somewhat mixed
on the location of the port. This meant that any typical cable would jut out of
the side. Thankfully, the included cable is right angled, more on that cable in
a bit. I find that the USB port is a bit too recessed and meant that some of my
USB C cables does not actually fit.

The Bluetooth connection of the keyboard is decent. There were a few times
where it suffers from stuck keys for a second and I'm not sure if that's an
issue with the keyboard or with the laptop or Windows. Other than that, the
latency is fine and the distance is good.


### The K6 65% Experience

It took a while to get used to the 65% layout, I'll admit.

Having the Function row be accessible via a function modifier took a while to
get used too since I do use those keys often. This meant that any modifiers
that involves the Function row takes more key presses.

Other thing I had to get used to are the placement of the backtick (\`) and
tilde (~). As mentioned before, the 65% layout combines Escape with Backtick
and Tilde. Escape being the default, with Fn 1 for backtick and Fn 2 for
tilde. Additionally, Fn 1 and shift would print a tilde character.

The biggest issue is that I use these two keys fairly often on Linux. Backtick
is used to switch between application windows and tilde is used to represent a
user's home directory. This meant that switching between application windows
will require two hands. My work computer uses Windows so I never had to use
these functions just yet, but it is something I had to keep in mind when using
the K6 in a Linux environment.

This only applies to the K6 and not to the other keyboards with 65% layouts but
the layout Keychron decided could be better. The rightmost column are the light
patterns, followed by home, page up and page down. I would have preferred if
the light patterns key did not have it's own dedicated key and be delegated to
a function key and have the end key be used instead. The End key of which is
accessed by using a modifier.

Since there isn't a standard layout for the rightmost column, it is possible
that some keycaps set does not fully cover the entire column.  The home key is
on R2 and most keycaps set have the home key on R1. Another example is the Fn 2
key. Other keyboards may have a single function modifier, thus we can use the
control key as a replacement for Fn 2.

Other keys such as Delete and Insert I don't mind being a modifier. Another
tidbit about the key layout is that the Keychron seems to be more Mac
orientated; as they have the same secondary functions as a Mac keyboard.

As of writing, there isn't an offical tool to change what the keys are mapped
to on a firmware level. This is a feature that I would love to see on the K6 as
it fixes some of the grimaces I have with the layout. There is a
[guide](https://github.com/CanUnesi/QMK-on-K6/blob/main/README.md) on flashing
QMK on the keyboard, but removes all bluetooth functionality, so it's a no go
for me.


### Modding

The Keychron K6 is very moddable. All of the internals are very easy to access
with standard screws and with the hot swappable PCB, it is is quite easy to mod
the switches. There are some keyboard mods that gets really involved such as
filming and lubing. But I want to keep it simple for now.

I did not like the included key cap puller as you have to bend the wires in
order to slide below the key caps. I've never used a switch puller before, but
it was a bit cumbersome to use the one included in the box. It's a small metal
arch that you pinch the legs of the switch to pull out. Because the tool is
made of metal, extra care has to be taken to remove the switch in order to
prevent scratching the painted metal plate.

Disassembling is very straightforward. Some hex screws on the side holds the
aluminium frame and 6 Phillips screws holding down the PCB and the metal plate.
Unfortunately, I don't have the necessary tools to remove the battery wire. It
is a JST 2-Pin connector and requires a pair of tweezers to unhook the
connector. With enough care however, it is possible to still work on the
keyboard with the battery plugged in, but I don't recommend it for safety
reasons.


### My Mods

The mods I've applied to the keyboard are simple. I did the masking tape mod,
shelf liner mod and switched out the key caps.

The masking tape mod is basically applying a few layers of masking tape to the
bottom of the PCB of the board. This apparently has the effect of making the
keyboard sound more poppy. The shelf liner mod involves filling the case with
shelf liner, which dampens the vibrations of the keyboard. This results in a
much more pleasant sound when typing.

I did whatever mod I could find and I think both the shelf liner mod and the
tape mod are actually counter productive. Still, both mods made the keyboard
sounds much nicer when typing.

I swapped out the key caps for a set of Akko Macaw PBT key caps. They are a
clone of the GMK Nautilus and are a pretty decent and affordable set. The
packaging they arrived in is neat and is presented nicely.

The only issues I have with the set are that it does not fully cover all of the
keys of the Keychron K6, as mentioned earlier. As such, I had to use the 1U
control key for Fn 2 and the minus key from the number pad as the home key.

However, the end result is still absolutely fantastic.


## Conclusion

I really like this keyboard. It is perfect for my use case, but there's some
improvements that could be make to make it an even better keyboard. But for
now, the positives outweighs the negatives. Now if only there's a wireless
version of the Keychron Q1...


## Gallery

<figure>
:::{.content-media-container}
![](k6-laptop.png "Keychron K6 on laptop")
:::

<figcaption>
Fits perfectly on a Dell Latitude 7420.
</figcaption>
</figure>

<figure>
:::{.content-media-container}
![](akko-keycaps-1.png "Akko Macaw packaging")
:::
:::{.content-media-container}
![](akko-keycaps-2.png "Akko Macaw packaging")
:::
:::{.content-media-container}
![](akko-keycaps-3.png "Akko Macaw key caps on Keychron K6")
:::

<figcaption>
Akko Macaw key caps set.
</figcaption>
</figure>

<figure>
:::{.content-media-container}
![](k6-nocaps.png "Keychron K6 without key caps")
:::

<figcaption>
K6 without key caps.
</figcaption>
</figure>

<figure>
:::{.content-media-container}
![](k6-switch.png "Gateron Brown switch")
:::

<figcaption>
K6 stabiliser and Gateron Brown switch.
</figcaption>
</figure>

<figure>
:::{.content-media-container}
![](k6-hotswap.png "Keychron K6 hot swap key switch")
:::

<figcaption>
Hot swappable key switches.
</figcaption>
</figure>

<figure>
:::{.content-media-container}
![](k6-final.png "My Keychron K6")
:::

<figcaption>
My Keychron K6, with Akko Macaw key caps and several mods.
</figcaption>
</figure>


<figure>
<div class="content-media-container">
<p>
<video controls>
<source src="k6-soundtest-1.webm" type="video/webm">
<source src="k6-soundtest-1.mp4" type="video/mp4">
</video>
</p>
</div>

<div class="content-media-container">
<p>
<video controls>
<source src="k6-soundtest-2.webm" type="video/webm">
<source src="k6-soundtest-2.mp4" type="video/mp4">
</video>
</p>
</div>

<figcaption>
Before and after sound comparison of the shelf liner and tape mod. Audio is
boosted by 20db. Recorded on an iPhone X.
</figcaption>
</figure>
