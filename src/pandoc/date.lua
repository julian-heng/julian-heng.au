function Meta(m)
    local datefmt = m.datefmt
    local datestr = pandoc.utils.stringify(m.date)

    if datefmt == nil then
        print(datestr)
        os.exit(0)
    end

    local y, m, d = datestr:match("(%d%d%d%d)-(%d%d)-(%d%d)")
    local date = os.time({year=y, month=m, day=d, hour=0})
    print(os.date(datefmt, date))
    os.exit(0)
end
