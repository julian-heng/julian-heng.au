function Pandoc(el)
    words = 0
    pandoc.walk_block(pandoc.Div(el.blocks), {
        Str = function(el)
            if el.text:match("%P") then
                words = words + 1
            end
        end,

        Code = function(el)
            _, n = el.text:gsub("%S+", "")
            words = words + n
        end,

        CodeBlock = function(el)
            _, n = el.text:gsub("%S+", "")
            words = words + n
        end
    })

    read_time_min = math.max(1, words / 250)
    read_time_min = string.format("%.0f", read_time_min)
    read_time_min = string.gsub(read_time_min, ".0", "")

    read_time_max = math.max(1, words / 200)
    read_time_max = string.format("%.0f", read_time_max)
    read_time_max = string.gsub(read_time_max, ".0", "")

    if read_time_min == read_time_max then
        read_time_max = ""
    end

    print(words, read_time_min, read_time_max)
    os.exit(0)
end
